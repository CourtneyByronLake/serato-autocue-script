# Serato-AutoCue-Script <a href="https://paypal.me/djceeb?country.x=GB&locale.x=en_GB" target="_blank"><img src="images/pplogo384.png" style="height: 41px; width: 41px"> </a>

An AppleScript to automate adding Cue Points In Serato DJ Pro. 

Here is the video tuorial:

The script adds cue points in the following places:

```
Cue Point 1: 0 (Start Of The Track)
Cue Point 2: 4 (End of The 4th Bar)
Cue Point 3: 8 (End Of The 8th Bar)
```

![Serato Auto Cue Demo](images/forgithub.gif)


## **Things You Need To Do Before Downloading The Script** 


1. Download Homebrew on your machine via terminal https://brew.sh/:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

2. Install CLIClick through Homebrew via terminal https://formulae.brew.sh/formula/cliclick: 
```
brew install cliclick
```

## **Installation Guide** 

*This will only work on Mac OS, I am trying to work on something that will work on a windows computer. So give me time.

1. Click the Blue Clone Button at the top of the page and click download zip
2. Unzip the folder
3. Open the Applescipt by double clicking it

## **BEFORE YOU RUN THIS SCRIPT** 
1. You run this program at your own risk. It works perfectly fine on my machine which doesnt mean it will 100% work on yours.
2. Always make sure you back up your computer and all your music just incase anything goes wrong.
3. Don’t change anything in the program (unless I tell you so in the tutorial)
4. If you change anything and your computer decides to mess up that all down to you.
5. I will not be responsible for anything that may happen to your computer if anything happens

## **How To Use** 

Before running the script ensure that you have installed brew and CLI click as stated above.

1. Open Serato DJ Pro
2. Create a crate with all the tracks you want to cue
3. Make sure all these tracks are analysed
4. Ensure Q at the top is enabled
5. Ensure the beat jump controls are enabled. (To enable: go to settings, DJ Preferences, Control Preferences, Show Beat Jump Controls)
6. You need to get the co-ordinates of the forward beat jump button. In the script it is currently set to (X=461, Y=206) These might be different to yours. But to check: 
```
On your keyboard press CMD + Shift + 4, this will bring up a cross with coordinates,
Place the cross over the Forward beat jump button and take note of the numbers:

The top number is X and the Bottom Number is Y.

```

7. Go to the apple script and update the BeatJumpForwardX and BeatJumpForwardY Numbers Here to what you have just noted down. (This is a one time activity)
8. Open up Serato DJ Pro and click the first track in your crate of tracks you want to cue
9. Go to the apple script and press play
10. This will open up Serato and give you a text box, enter the amount of tracks inside this crate and press okay.

## **THIS SCRIPT WILL NOT WORK IF** 

1. You havent anaylsyed your tracks
2. You dont do everything I mentioned in the previous steps
